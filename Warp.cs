﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

/*
 * Interface for all objects that can move from a zone to another
 */
public interface Warp {

    /*
     * Returns the target zone of the warp
     */
    Zone TargetZone();

    /*
     * Returns the target position of the warp
     */
    Position TargetPosition();
}


/*
 * This class represents an interactable object that warps you
 */
public abstract class WarpTile : Tile, Warp {

    private Func<Zone> zoneSupplier;
    private Position position;

    protected WarpTile(Func<Zone> zoneSupplier, Position position) : base() {
        this.zoneSupplier = zoneSupplier;
        this.position = position;
    }

    public Zone TargetZone() {
        return zoneSupplier.Invoke();
    }

    public Position TargetPosition() {
        return position;
    }

}

/*
 * Represents a tile that warps you
 */
public abstract class WarpObstacle : Interactable, Warp {

    private Func<Zone> zoneSupplier;
    private Position position;

    protected WarpObstacle(Func<Zone> zoneSupplier, Position position, Direction direction, bool allDirections) : base(direction, allDirections) {
        this.zoneSupplier = zoneSupplier;
        this.position = position;
    }

    public Zone TargetZone() {
        return zoneSupplier.Invoke();
    }

    public Position TargetPosition() {
        return position;
    }
}

/*
 * Represents a transition between two places
 */
public class Transition : WarpTile {

    public Transition(Func<Zone> zoneSupplier, Position position) : base(zoneSupplier, position) {

    }
}

/*
 * Represents a door
 */
public class Door : WarpObstacle {

    public Door(Func<Zone> zoneSupplier, Position position, Direction direction) : base(zoneSupplier, position, direction, false) {

    }

    public override void Activate() {}

}

