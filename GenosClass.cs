﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* The type chart is as follows :
 *  |     | HLY | DVL | SPR | BST | INS | AVN | PLT | AQN | SCL | BRW | PLG | SLM | ENR | MCH | HMN | ALN | MYT |
 *  | HLY | 1/2 |  2  | 1/2 |  1  | 1/2 |  1  |  1  |  1  |  1  |  1  |  2  |  2  |  1  |  1  |  2  |  1  | 1/2 |
 *  | DVL | 1/2 | 1/2 |  2  |  1  |  1  |  1  |  1  |  1  |  1  |  1  |  1  |  2  |  1  |  2  |  2  |  2  | 1/2 |
 *  | SPR |  2  | 1/2 | 1/2 |  1  |  1  |  1  |  1  |  1  |  1  |  1  |  1  |  2  | 1/2 |  2  | 1/2 |  1  | 1/2 |
 *  | BST |  1  |  1  |  1  | 1/2 |  1  |  2  |  1  |  2  |  1  |  1  |  1  |  1  |  1  | 1/2 |  1  |  1  |  1  |
 *  | INS |  1  |  1  |  1  |  2  |  1  |  1  |  2  | 1/2 |  1  | 1/2 | 1/2 |  2  |  1  | 1/2 |  2  |  2  |  1  |
 *  | AVN |  1  |  1  |  1  | 1/2 |  2  |  1  |  2  |  2  |  2  | 1/2 |  1  |  1  | 1/2 | 1/2 |  1  |  1  |  1  |
 *  | PLT |  1  |  2  |  1  |  1  |  1  | 1/2 |  1  |  2  |  1  |  1  | 1/2 | 1/2 |  2  | 1/2 |  1  |  1  |  1  |
 *  | AQN |  1  |  1  |  1  |  1  |  2  |  1  | 1/2 |  1  |  1  | 1/2 |  1  |  2  |  1  |  1  |  1  |  2  |  1  |
 *  | SCL |  2  |  1  |  1  | 1/2 |  2  | 1/2 |  1  |  1  |  1  |  2  |  1  |  1  |  2  | 1/2 |  1  |  1  |  1  |
 *  | BRW |  1  |  1  |  1  |  1  |  2  | 1/2 | 1/2 |  0  |  1  |  1  |  1  |  2  |  2  |  1  |  1  |  1  |  1  |
 *  | PLG |  2  |  1  |  0  |  2  |  2  |  2  |  2  |  2  |  2  |  2  |  0  |  0  |  0  |  0  |  2  | 1/2 |  2  |
 *  | SLM | 1/2 |  1  |  1  |  1  | 1/2 |  1  |  1  |  1  |  1  |  1  |  2  | 1/2 |  2  |  2  |  1  |  1  | 1/2 |
 *  | ENR |  1  |  2  |  2  |  1  |  1  |  2  | 1/2 |  1  | 1/2 |  1  |  2  |  1  |  1  |  2  |  1  |  1  | 1/2 |
 *  | MCH | 1/2 | 1/2 |  1  |  2  | 1/2 |  1  |  1  | 1/2 |  1  |  2  |  1  | 1/2 |  1  |  1  |  1  |  1  |  1  |
 *  | HMN |  1  |  2  | 1/2 |  1  | 1/2 |  1  |  1  |  1  |  1  |  1  |  1  |  1  |  1  |  1  |  1  |  2  |  2  |
 *  | ALN |  2  |  1  |  1  |  1  |  1  |  1  |  1  |  1  |  1  | 1/2 |  1  | 1/2 |  2  |  2  | 1/2 |  1  |  2  |
 *  | MYT |  2  |  2  |  2  |  1  |  1  |  1  | 1/2 |  1  |  1  |  1  |  1  | 1/2 |  2  |  1  | 1/2 |  0  |  2  |
 */

/*
 * This interface represents a Genos class
 * Every individual Genos class implements this class and contains a private contructor with a single static instance of this class
 */
public interface GenosClass {

    // the name and symbol of the class
    string Name {get;}
    string Symbol {get;}

    /*
     * The effectiveness of an attack of this class against the given class
     */
    double Effectiveness(GenosClass target);
}

/*
 * Then, we create each Genos class
 */
public class Holy : GenosClass {

    public static GenosClass HLY = new Holy();

    public string Name {get{return "Holy";}}
    public string Symbol {get{return "HLY";}}

    private Holy() {}

    public double Effectiveness(GenosClass target) {
        switch(target.Symbol) {
            case "HLY":
            case "SPR":
            case "INS":
            case "MYT":
                return 1/2;
            case "DVL":
            case "PLG":
            case "SLM":
            case "ENR":
                return 2;
            default: return 1;
        }
    }
}

public class Devil : GenosClass {

    public static GenosClass DVL = new Devil();

    public string Name {get{return "Devil";}}
    public string Symbol {get{return "DVL";}}

    private Devil() {}

    public double Effectiveness(GenosClass target) {
        switch (target.Symbol) {
            case "HLY":
            case "DVL":
            case "MYT":
                return 1/2;
            case "SPR":
            case "SLM":
            case "MCH":
            case "HMN":
            case "ALN":
                return 2;
            default: return 1;
        }
    }
}

public class Spirit : GenosClass {

    public static GenosClass SPR = new Spirit();

    public string Name {get{return "Spirit";}}
    public string Symbol {get{return "SPR";}}

    private Spirit() {}

    public double Effectiveness(GenosClass target) {
        switch (target.Symbol) {
            case "DVL":
            case "SPR":
            case "ENR":
            case "HMN":
            case "MYT":
                return 1/2;
            case "HLY":
            case "SLM":
            case "MCH":
                return 2;
            default: return 1;
        }
    }
}

public class Beast : GenosClass {

    public static GenosClass BST = new Beast();

    public string Name {get{return "Beast";}}
    public string Symbol {get{return "BST";}}

    private Beast() {}

    public double Effectiveness(GenosClass target) {
        switch (target.Symbol) {
            case "BST":
            case "MCH":
                return 1/2;
            case "AVN":
            case "AQN":
                return 2;
            default: return 1;
        }
    }
}

public class Insect : GenosClass {

    public static GenosClass INS = new Insect();

    public string Name {get{return "Insect";}}
    public string Symbol {get{return "INS";}}

    private Insect() {}

    public double Effectiveness(GenosClass target) {
        switch (target.Symbol) {
            case "AQN":
            case "BRW":
            case "PLG":
            case "MCH":
                return 1/2;
            case "BST":
            case "PLT":
            case "SLM":
            case "HMN":
            case "ALN":
                return 2;
            default: return 1;
        }
    }
}

public class Avian : GenosClass {

    public static GenosClass AVN = new Avian();

    public string Name {get{return "Avian";}}
    public string Symbol {get{return "AVN";}}

    private Avian() {}

    public double Effectiveness(GenosClass target) {
        switch (target.Symbol) {
            case "BST":
            case "BRW":
            case "ENR":
            case "MCH":
                return 1/2;
            case "INS":
            case "PLT":
            case "AQN":
            case "SCL":
                return 2;
            default: return 1;
        }
    }
}

public class Plant : GenosClass {

    public static GenosClass PLT = new Plant();

    public string Name {get{return "Plant";}}
    public string Symbol {get{return "PLT";}}

    private Plant() {}

    public double Effectiveness(GenosClass target) {
        switch (target.Symbol) {
            case "AVN":
            case "PLG":
            case "SLM":
            case "PLT":
                return 1/2;
            case "DVL":
            case "AQN":
            case "ENR":
                return 2;
            default: return 1;
        }
    }
}

public class Aquan : GenosClass {

    public static GenosClass AQN = new Aquan();

    public string Name {get{return "Aquan";}}
    public string Symbol {get{return "AQN";}}

    private Aquan() {}

    public double Effectiveness(GenosClass target) {
        switch (target.Symbol) {
            case "PLT":
            case "BRW":
                return 1/2;
            case "INS":
            case "SLM":
            case "ALN":
                return 2;
            default: return 1;
        }
    }
}

public class Scalie : GenosClass {

    public static GenosClass SCL = new Scalie();

    public string Name {get{return "Scalie"; }}
    public string Symbol {get{return "SCL"; }}

    private Scalie() {}

    public double Effectiveness(GenosClass target) {
        switch (target.Symbol) {
            case "BST":
            case "AVN":
            case "MCH":
                return 1/2;
            case "HLY":
            case "INS":
            case "BRW":
            case "ENR":
                return 2;
            default: return 1;
        }
    }
}

public class Burrower : GenosClass {

    public static GenosClass BRW = new Burrower();

    public string Name {get{return "Burrower";}}
    public string Symbol {get{return "BRW";}}

    private Burrower() {}

    public double Effectiveness(GenosClass target) {
        switch (target.Symbol) {
            case "AQN": return 0;
            case "AVN":
            case "PLT":
                return 1/2;
            case "INS":
            case "SLM":
            case "ENR":
                return 2;
            default: return 1;
        }
    }
}

public class Plague : GenosClass {

    public static GenosClass PLG = new Plague();

    public string Name {get{return "Plague";}}
    public string Symbol {get{return "PLG";}}

    private Plague() {}

    public double Effectiveness(GenosClass target) {
        switch (target.Symbol) {
            case "SPR":
            case "PLG":
            case "SLM":
            case "ENR":
            case "MCH":
                return 0;
            case "ALN":
                return 1/2;
            case "HLY":
            case "BST":
            case "INS":
            case "AVN":
            case "PLT":
            case "AQN":
            case "SCL":
            case "BRW":
            case "HMN":
            case "MYT":
                return 2;
            default: return 1;
        }
    }
}

public class Slime : GenosClass {

    public static GenosClass SLM = new Slime();

    public string Name {get{return "Slime";}}
    public string Symbol {get{return "SLM";}}

    private Slime() {}

    public double Effectiveness(GenosClass target) {
        switch (target.Symbol) {
            case "HLY":
            case "DVL":
            case "INS":
            case "SLM":
                return 1/2;
            case "PLG":
            case "ENR":
            case "MCH":
                return 2;
            default: return 1;
        }
    }
}

public class Energy : GenosClass {

    public static GenosClass ENR = new Energy();

    public string Name {get{return "Energy"; }}
    public string Symbol {get{return "ENR"; }}

    private Energy() {}

    public double Effectiveness(GenosClass target) {
        switch (target.Symbol) {
            case "PLT":
            case "SCL":
            case "MYT":
                return 1/2;
            case "DVL":
            case "SPR":
            case "AVN":
            case "PLG":
            case "MCH":
                return 2;
            default: return 1;
        }
    }
}

public class Machine : GenosClass {

    public static GenosClass MCH = new Machine();

    public string Name {get{return "Machine";}}
    public string Symbol {get{return "MCH";}}

    private Machine() {}

    public double Effectiveness(GenosClass target) {
        switch (target.Symbol) {
            case "HLY":
            case "INS":
            case "AQN":
            case "SLM":
                return 1/2;
            case "BST":
            case "BRW":
                return 2;
            default: return 1;
        }
    }
}

public class Homonculus : GenosClass {

    public static GenosClass HMN = new Homonculus();

    public string Name {get{return "Homonculus";}}
    public string Symbol {get{return "HMN";}}

    private Homonculus() {}

    public double Effectiveness(GenosClass target) {
        switch (target.Symbol) {
            case "SPR":
            case "INS":
                return 1 / 2;
            case "DVL":
            case "ALN":
            case "MYT":
                return 2;
            default: return 1;
        }
    }
}

public class Alien : GenosClass {

    public static GenosClass ALN = new Alien();

    public string Name {get{return "Alien";}}
    public string Symbol {get{return "ALN";}}

    private Alien() {}

    public double Effectiveness(GenosClass target) {
        switch (target.Symbol) {
            case "BRW":
            case "SLM":
            case "HMN":
                return 1 / 2;
            case "HLY":
            case "ENR":
            case "MCH":
            case "MYT":
                return 2;
            default: return 1;
        }
    }
}

public class Mythical : GenosClass {

    public static GenosClass MYT = new Mythical();

    public string Name {get{return "Mythical";}}
    public string Symbol {get{return "MYT";}}

    private Mythical() {}

    public double Effectiveness(GenosClass target) {
        switch (target.Symbol) {
            case "ALN": return 0;
            case "PLT":
            case "HMN":
                return 1/2;
            case "HLY":
            case "DVL":
            case "SPR":
            case "ENR":
                return 2;
            default: return 1;
        }
    }
}