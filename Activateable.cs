﻿using System;
using System.Collections;
using System.Collections.Generic;

/* 
 * This class represents something that can be activated
 */
public abstract class Activateable {

    // The name and the description of the activateable
    public string Name {get;}
    public string Description {get;}

    // The list of effects for this activateable
    public List<Effect> Effects {get; private set;}

    protected Activateable(string name, string description) {
        this.Name = name;
        this.Description = description;
        // we initialize all the effects as null
        this.Effects = new List<Effect>();
    }

    /*
     * Adds an effect via a list of function that get an Effect from a Field
     */
    protected Activateable AddEffect(Effect effect) {
        Effects.Add(effect);
        return this;
    }
}