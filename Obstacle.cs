﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using UnityEngine;

/*
 * This class represents an obstacle in the overworld
 */
public interface Obstacle : OverworldObject {

}

/*
 * This class represents an obstacle you can interact with
 */
public abstract class Interactable : Obstacle {

    // if the object can be interacted with from all sides
    private bool allDirections;

    // the orientation of the object
    protected Direction Orientation {get; set;}

    protected Interactable(Direction direction, bool allDirections) {
        this.Orientation = direction;
        this.allDirections = allDirections;
    }

    /*
     * activates the interactable
     */
    public abstract void Activate();

    /*
     * checks if the obstacle can be interacted with from this direction
     */
    public bool IsInteractableFrom(Direction direction){
        return allDirections || DirectionExtension.AreOpposites(Orientation, direction);
    }
}

/*
 * class for an NPC
 */
public class NPC : Interactable {

    public string Name {get; set;}
    public List<PronounSet> Pronouns {get; set;}
    public List<string> Dialogue {get; set;}

    public NPC(string name, List<PronounSet> pronouns, List<string> dialogue, Direction direction) : base(direction, true) {
        this.Name = name;
        this.Pronouns = pronouns;
        this.Dialogue = dialogue;
    }

    /*
     * Displays the dialogue
     */
    public override void Activate() {
        // TODO
    }

}

/*
 * class for a set of pronouns
 */
public class PronounSet {

    public string Sub {get; set;}
    public string Obj {get; set;}
    public string Pos {get; set;}

    public PronounSet(string sub, string obj, string pos) {
        this.Sub = sub;
        this.Obj = obj;
        this.Pos = pos;
    }
}
