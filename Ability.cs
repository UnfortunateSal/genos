﻿using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;

/*
 * This class represents an ability (name subject to change)
 */
public class Ability : Activateable {

    // all the abilities go here
    public static Ability HOLY_HEALING = new Ability("Holy Healing",
        "Any Genos that is sent in directly after this Genos will be healed on the end of the turn in which it is sent in")

    // Moment: After a move
        .AddEffect(new Effect(Moment.AFTER_MOVE)
    // Condition: Genos with this ability is switching out
            .AddCondition(new MoveTypeCondition<Switch>(true))
    // Result: Adds the status Holy Healing to the field
            .AddResult(new FieldStatusResult(
                new EffectStatus(
    // Moment: At the end of the turn
                    new Effect(Moment.END_OF_TURN)
    // Result: Genos is healed by 25%
                        .AddResult(new HealthResult(25, true)),
    // Duration: Until the end of the turn
                    0,
                    false),
                true)));

    public static Ability PROTECTOR = new Ability("Protector",
            "This Geno will absorb 10% of the damage dealt to any of its teammates, provided it has already entered the field of battle")
    // Moment: Start of turn
        .AddEffect(new Effect(Moment.START_OF_TURN)
    // Condition: Genos with this ability is switching out
            .AddCondition(new MoveTypeCondition<Switch>(true))
    // Result: Adds the status Protected to the field
            .AddResult(new ProtectedResult()))

    // Moment: After an attack
        .AddEffect(new Effect(Moment.AFTER_MOVE)
    // Condition: Genos with this ability is switching in
            .AddCondition(new MoveTypeCondition<Switch>(true))
    // Result: Removes the status Protected from the field
            .AddResult(new RemoveStatusResult<Protected>(true)));

    private Ability(string name, string description) : base(name, description) {}

    private new Ability AddEffect(Effect effect) {
        return (Ability) base.AddEffect(effect);
    }
}
