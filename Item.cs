﻿using System.Collections;
using System.Collections.Generic;
/*
 * This class represents an item (name susceptible to change)
 */
public abstract class Item : Activateable {

    // True if the item is consumable, false otherwise
    public bool IsConsumable {get;}

    protected Item(string name, string description, bool consumable) : base(name, description) {
        this.IsConsumable = consumable;
    }
}

/*
 * This class represents a holdable item (name susceptible to change)
 */
public class Holdable : Item  {

    /*
     * All of the holdable items go here
     */
    // this item represents no held item
    public static Holdable NONE = null;

    public static Holdable VACCINE = new Holdable("Vaccine", "Makes the holder immune to the plague", false)
    // Moment: Start of turn
        
        .AddEffect(new Effect(Moment.START_OF_TURN)
    // Condition: Plagued Immunity Status is not active on the Genos (StatusCondition)
            .AddCondition(new StatusCondition<ImmunityStatus<Plagued>>(false, true))
    // Result: Genos gets the Plague Immunity Status
            .AddResult(new GenosStatusResult(new ImmunityStatus<Plagued>(-1), true)));

    public Holdable(string name, string description, bool consumable) : base(name, description, consumable) { }

    private new Holdable AddEffect(Effect effect) {
        return (Holdable) base.AddEffect(effect);
    }
}
