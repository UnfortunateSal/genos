﻿using System.Collections;
using System.Collections.Generic;
using System.IO.Pipes;
using UnityEngine;

/*
 * interface for a zone
 */
public abstract class Zone {

    // the array of tiles in the area
    private Tile[,] tiles;

    // the onstacle at each position in the area
    private Dictionary<Position, Obstacle> obstacles;

    protected Zone(Tile[,] tiles, Dictionary<Position, Obstacle> obstacles){
        this.tiles = tiles;
        this.obstacles = obstacles;
    }

    /*
     * returns the obstacle in front of the given position in the given direction
     */
    public Obstacle InFront(Position position, Direction direction) {
        Obstacle obstacle = null;
        position.Move(direction);
        obstacles.TryGetValue(position, out obstacle);
        return obstacle;
    }

    /*
     * Returns the tile at the given position
     */
    public Tile TileAt(Position position) {
        return tiles[position.X, position.Y];
    }
}

/*
 * default area
 */
public class Home : Zone {

    // The relevant tiles and obstacles are declared here
    private static Tile G = Tile.GRASS;
    private static Tile P = Tile.PATH;
    private static Tile V = new Transition(() => new Home(), new Position(9, 2));
    private static Tile W = new Transition(() => new Home(), new Position(0, 2));
    private static NPC LesbianOne = new NPC("Lesbian One",
                    new List<PronounSet> {
                        new PronounSet("she", "her", "her"),
                        new PronounSet("they", "them", "their")
                    },
                    new List<string> {
                        "I'm a bisexual lesbian!",
                        "I'm also nonbinary!",
                        "If you call me female-aligned I will beat the shit out of you!",
                        "Have a nice day!"
                    },
                    Direction.DOWN);
    private static NPC LesbianTwo = new NPC("Lesbian Two",
                    new List<PronounSet> {
                        new PronounSet("he", "him", "his"),
                        new PronounSet("it", "it", "its")
                    },
                    new List<string> {
                        "I'm a he/him lesbian!",
                        "I also use it pronouns!",
                        "If you have a problem with that, you can choke on my nonbinary dick!",
                        "Have a nice day!"
                    },
                    Direction.UP);

    // the tiles and obstacles get passed directly in the constructor
    public Home() : base(
        new Tile[10, 5] {
            {G,G,V,G,G},
            {G,G,P,G,G},
            {G,G,P,G,G},
            {G,G,P,G,G},
            {G,G,P,G,G},
            {G,G,P,G,G},
            {G,G,P,G,G},
            {G,G,P,G,G},
            {G,G,P,G,G},
            {G,G,W,G,G}
        },
        new Dictionary<Position, Obstacle>() {
            {new Position(1, 2), LesbianOne},
            {new Position(8, 2), LesbianTwo}
        }) {}
}
