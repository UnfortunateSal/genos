﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

/*
 * Interface for a battleable object
 */
public interface Field {

    // The list of statuses that affect the field
    List<Status> FieldStatus {get; set;}

    // The list of statuses that affect the field and the genos
    List<Status> AllStatus {get;}

    // The currently queued move
    // When 2 opponents have a move queued, the turn will be performed by the Battle class
    Move QueuedMove {get; set;}

    // This is true if the trainer is ready for battle, false otherwise
    bool ReadyForBattle {get;}

    // The current team of the Genos
    List<Genos> Team {get;}

    // The Genos that is currently out
    Genos Out {get;}

    // True if the field has lost, false otherwise
    bool HasLost {get;}

    // The move's priority bracket
    int PriorityBracket {get;}

    /*
     * Waits until a move is selected, or, in the case of the player, a command is selected
     */
    void WaitForMove();
}

/*
 * This interface represents a field which uses AI to battle
 */
public interface AIField : Field {

    /*
     * This is the list of all possible moves for the AI field, useful for iterating through all the availiable moves to see which is the most effective
     */
    List<Move> PossibleMoves();
}

/*
 * Class for all Trainer NPCs
 */
public class Trainer : NPC, AIField {

    public List<Status> FieldStatus {get; set;}

    public List<Status> AllStatus {get{return Out.GenosStatus.Concat(FieldStatus).ToList();}}

    public Move QueuedMove {get; set;}

    public bool ReadyForBattle {get; private set;}

    public List<Genos> Team {get; private set;}

    public Genos Out {get{return Team[0];} set{Team[0] = value;}}

    public bool HasLost {get{return Team.TrueForAll(Genos => Genos.CurrentHealth == 0);}}

    public int PriorityBracket{get{
            int bracket = 0;
            foreach (Status status in AllStatus) {
                // if the status is a status affecting priority
                if (status is PriorityStatus) {
                    bracket += ((PriorityStatus) status).PriorityBracket;
                }
            }
            return bracket;}}

    public Trainer(string name, List<PronounSet> pronouns, List<string> dialogue, Direction direction, params Genos[] team) : base(name, pronouns, dialogue, direction) {
        FieldStatus = new List<Status>();
        QueuedMove = null;
        ReadyForBattle = true;
        Team = new List<Genos>(team);
    }

    public List<Move> PossibleMoves() {
        List<Move> moves = new List<Move>(Out.Moveset);
        for (int i = 1; i < Team.Count; i++) {
            moves.Add(new Switch(i));
        }
        return moves;
    }

    public void WaitForMove() {}

}

/*
 * Class for all tiles that can provoke a wild battle
 */
public class WildBattleTile : Tile, Field {

    public List<Status> FieldStatus {get; set;}

    public List<Status> AllStatus {get{return Out.GenosStatus.Concat(FieldStatus).ToList(); }}

    public Move QueuedMove {get; set;}

    // wild battle tiles are always ready for battle
    public bool ReadyForBattle {get{return true;}}

    // the team if of only one genos
    public List<Genos> Team {get{return new List<Genos>() {Out};} set{Out = value[0];}}

    public Genos Out {get; set;}

    public bool HasLost {get{return Out.CurrentHealth == 0;}}

    public int PriorityBracket{get{
            int bracket = 0;
            foreach (Status status in AllStatus) {
                // if the status is a status affecting priority
                if (status is PriorityStatus) {
                    bracket += ((PriorityStatus)status).PriorityBracket;
                }
            }
            return bracket;}}

    public WildBattleTile(Genos genos) {
        Out = genos;
        FieldStatus = new List<Status>();
    }

    public List<Move> PossibleMoves() {
        List<Move> moves = new List<Move>();
        moves.Add(Move.BASH);
        moves.Add(Move.THROW);
        moves.Add(Move.GUARD);
        moves.Add(Move.REST);
        foreach (Move move in Out.Moveset) {
            moves.Add(move);
        }
        return moves;
    }

    // wild Genos pick a random move
    public void WaitForMove() {
        System.Random random = new System.Random();
        QueuedMove = PossibleMoves()[random.Next(PossibleMoves().Count)];
    }

}

