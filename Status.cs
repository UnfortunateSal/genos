﻿using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;

/*
 * This class represents a status, essentially, anything that can affect a Genos or a Field
 */
public class Status {

    // Duration of the status (-1 if infinite)
    public int Duration {get;}
    // Current duration of the status
    public int CurrentDuration {get; set;}
    // if the status is visible
    public bool IsVisible {get;}

    public Status(int duration, bool isVisible) {
        this.Duration = duration;
        this.CurrentDuration = 0;
        this.IsVisible = false;
    }

    /*
     * Returns true if the status should be removed, false otherwise
     */
    public bool ToBeRemoved() {
        return Duration >= 0 || CurrentDuration >= Duration;
    }

}

/*
 * This class represents a status that causes an effect
 */
public class EffectStatus : Status {
    // The effect of the status
    public Effect StatusEffect {get;}

    public EffectStatus(Effect effect, int duration, bool isVisible) : base(duration, isVisible) {
        this.StatusEffect = effect;
    }
}

/*
 * This class represents the Plagued status
 */
public class Plagued : EffectStatus {

    public Plagued(int duration) : base(
        // Moment: End of turn
        // Condition: None
        // Result: Genos loses 10% of its max HP (HealthResult)
        new Effect(Moment.END_OF_TURN)
            .AddResult(new HealthResult(-10, true)), 
        duration, true) {}
}

/*
 * This class represents the Protected status, a special status that gets handled in DamageResult
 */
public class Protected : Status {

    // the team position of the protector
    public int Index {get;}

    // The Protected status lasts until the end of the turn
    public Protected(int index) : base(0, false) {
        Index = index;
    }
}

/*
 * This class represents a status that moves the owner to a given priority bracket for one turn
 */
public class PriorityStatus : Status {
    // The priority bracket of the owner
    public int PriorityBracket {get;}

    public PriorityStatus(int priorityBracket) : base(0, false) {
        this.PriorityBracket = priorityBracket;
    }
}

/*
 * This class represents a status that changes the stats of the owner for one turn
 */
public class StatsStatus : Status {
    // The stat and its multiplier
    public string Stat {get;}
    public double Multiplier {get;}

    public StatsStatus(string stat, double multiplier) : base(0, false) {
        this.Stat = stat;
        this.Multiplier = multiplier;
    }
}

/*
 * This class represents a status that changes the stamina cost of attacks of the owner or the opponent
 */
public class StaminaCostStatus : Status {
    public double Multiplier {get;}

    public StaminaCostStatus(int multiplier) : base(0, false) {
        this.Multiplier = multiplier;
    }
}

/*
 * This class represents a status that changes the stamina regeneration of the owner or the opponent
 */
public class StaminaRegenerationStatus : Status {
    public double Multiplier {get;}

    public StaminaRegenerationStatus(int multiplier) : base(0, false) {
        this.Multiplier = multiplier;
    }
}

/*
 * This class represents a status that prevents another type of status from being inflicted regeneration of the owner or the opponent
 */
public class ImmunityStatus<T> : Status {

    public ImmunityStatus(int duration) : base(duration, false) {}
}
