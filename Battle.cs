﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

/*
 * This class represents a battle
 */
public class Battle {

    // true if the battle is over, false otherwise
    private bool battleOver;

    // The opponent
    public Field Opponent {get; private set;}
    

    /*
     * This constructor performs the battle
     */
    public Battle(Field opponent) {
        this.Opponent = opponent;
        battleOver = false;
        while (!battleOver) {
            // waits for the moves (async?)
            // waits for the player first
            Player.PLAYER.WaitForMove();
            // then waits for the opponent
            if (Opponent is AIField) {
                // do all the AI bullshit
            }
            else {
                Opponent.WaitForMove();
            }
            // if the player is faster, its move gets performed first
            if (PlayerFaster()) {
                // we perform the turn
                PerformTurn(true);
            }
            else {
                // we perform the turn
                PerformTurn(false);
            }
            // if one of the players has lost, the battle is over
            if(Player.PLAYER.HasLost || Opponent.HasLost) {
                battleOver = true;
            }
        }
        Heal();
    }

    /*-------------------------------------------------------------------------------------------------------------------------
     Private functions for battles
    -------------------------------------------------------------------------------------------------------------------------*/

    /*
     * Returns true if the player moves faster than the opponent, false otherwise
     */
    private bool PlayerFaster() {
        // first we check priority brackets
        if (Player.PLAYER.PriorityBracket > Opponent.PriorityBracket) {
            return true;
        }
        else if (Player.PLAYER.PriorityBracket < Opponent.PriorityBracket) {
            return false;
        }
        // then we check the actual speed stat
        double playerMultiplier = 1;
        double opponentMultiplier = 1;
        // we check through the player status
        foreach (Status status in Player.PLAYER.AllStatus) {
            // if the status is a status affecting speed
            if (status is StatsStatus && ((StatsStatus) status).Stat == "RT") {
                playerMultiplier *= ((StatsStatus) status).Multiplier;
            }
        }
        // we check through the opponent status
        foreach (Status status in Player.PLAYER.AllStatus) {
            // if the status is a status affecting speed
            if (status is StatsStatus && ((StatsStatus) status).Stat == "RT") {
                opponentMultiplier *= ((StatsStatus) status).Multiplier;
            }
        }
        if (Player.PLAYER.Out.GenosStats.RT.Value * playerMultiplier > Opponent.Out.GenosStats.RT.Value * opponentMultiplier) {
            return true;
        }
        if (Player.PLAYER.Out.GenosStats.RT.Value * playerMultiplier < Opponent.Out.GenosStats.RT.Value * opponentMultiplier) {
            return false;
        }
        Random random = new Random();
        return random.Next(2) == 0;
    }

    /*
     * Heals the player's team
     */
    private void Heal() {
        foreach(Genos genos in Player.PLAYER.Team) {
            genos.CurrentHealth = genos.GenosStats.H.BaseValue;
        }
    }

    /*
     * Performs the turn, with playerGoesFirst being true if the player goes first
     */
    private void PerformTurn(bool playerGoesFirst)  {
        // we apply all the effects that occur at the start of the turn
        ApplyAllEffects(playerGoesFirst, Moment.START_OF_TURN);
        ApplyAllEffects(!playerGoesFirst, Moment.START_OF_TURN);
        PlayerAttack(playerGoesFirst);
        // we apply all the effects that occur after the first attack
        ApplyAllEffects(playerGoesFirst, Moment.AFTER_MOVE);
        PlayerAttack(!playerGoesFirst);
        // we apply all the effects that occur after the second attack
        ApplyAllEffects(!playerGoesFirst, Moment.AFTER_MOVE);
        // we apply all the effects that occur at the end of the turn
        ApplyAllEffects(playerGoesFirst, Moment.END_OF_TURN);
        ApplyAllEffects(!playerGoesFirst, Moment.END_OF_TURN);
    }

    /* 
     * Makes the player perform the queued move against the opponent
     * playerAttacking is true if the player is attacking, false otherwise
     */
    private void PlayerAttack(bool playerAttacking) {
        if (playerAttacking) {
            Move move = Player.PLAYER.QueuedMove;
            if (move == Move.RUN) {
                // TODO: try to run
                // if we sucessfully run
                battleOver = true;
            }
            else if (move == Move.SCAN) {
                // TODO: try to scan the Genos
                // if the Genos has been caught
                battleOver = true;
            }
            else {
                // if the move is damaging
                if (move is Technique) {
                    Technique technique = (Technique) move;
                    if (technique is DamagingTechnique) {
                        DamagingTechnique damagingTechnique = (DamagingTechnique) technique;
                        // we find the damage multiplier for the status
                        double damageMultiplier = StatusDamageMultiplier(damagingTechnique, playerAttacking);
                        // the opposing Genos takes damage
                        Opponent = new DamageResult(Player.PLAYER.Out, damagingTechnique, damageMultiplier, false).Apply(Opponent);
                    }
                    // we find the stamina multiplier for the status
                    double StaminaMultiplier = 1;
                    foreach (Status status in Player.PLAYER.AllStatus) {
                        // if the status is a status affecting the stamina of the owner
                        if (status is StaminaCostStatus) {
                            StaminaMultiplier *= ((StaminaCostStatus) status).Multiplier;
                        }
                    }
                    // the attacking Genos loses stamina
                    new StaminaResult(Convert.ToInt32(-technique.StaminaCost * StaminaMultiplier), true).Apply(Player.PLAYER);
                }
            }
        }
        else {
            Move move = Opponent.QueuedMove;
            // if the move is damaging
            if (move is Technique) {
                Technique technique = (Technique) move;
                if (move is DamagingTechnique) {
                    DamagingTechnique damagingTechnique = (DamagingTechnique) move;
                    // we find the multiplier for the status
                    double damageMultiplier = StatusDamageMultiplier(damagingTechnique, playerAttacking);
                    // the opposing Genos takes damage
                    Player.PLAYER = (Player) new DamageResult(Opponent.Out, damagingTechnique, damageMultiplier, false).Apply(Player.PLAYER);
                }
                // we find the stamina multiplier for the status
                double StaminaMultiplier = 1;
                foreach (Status status in Opponent.AllStatus) {
                    // if the status is a status affecting the stamina of the owner
                    if (status is StaminaCostStatus) {
                        StaminaMultiplier *= ((StaminaCostStatus) status).Multiplier;
                    }
                }
                // the attacking Genos loses stamina
                Opponent = new StaminaResult(Convert.ToInt32(-technique.StaminaCost * StaminaMultiplier), true).Apply(Opponent);
            }
        }
    }

    /*
     * Applies all of the effects of a move, item or ability
     * belongsToPlayer is true if we are applying the effects beloning to the player
     */
    private void ApplyAllEffects(bool belongsToPlayer, Moment moment) {
        if(belongsToPlayer) {
            // if the player is attacking
            // for each effect from the ability
            foreach (Effect effect in Player.PLAYER.Out.GenosAbility.Effects) {
                // if the effect matches the moment we apply it
                if (moment == effect.EffectMoment) {
                    Opponent = effect.Apply(this, belongsToPlayer);
                }
            }
            // for each effect from the item
            foreach (Effect effect in Player.PLAYER.Out.HeldItem.Effects) {
                // if the effect matches the moment we apply it
                if (moment == effect.EffectMoment) {
                    Opponent = effect.Apply(this, belongsToPlayer);
                }
            }
            // for each effect from the move
            foreach (Effect effect in Player.PLAYER.QueuedMove.Effects) {
                // if the effect matches the moment we apply it
                if (moment == effect.EffectMoment) {
                    Opponent = effect.Apply(this, belongsToPlayer);
                }
            }
            // for each effect from the statuses
            foreach (Status status in Player.PLAYER.AllStatus) {
                if (status is EffectStatus) {
                    Effect effect = ((EffectStatus) status).StatusEffect;
                    if (moment == effect.EffectMoment) {
                        Opponent = effect.Apply(this, belongsToPlayer);
                    }
                }
            }
        }
        else {
            // if the opponent is attacking
            // for each effect from the ability
            foreach (Effect effect in Opponent.Out.GenosAbility.Effects) {
                // if the effect matches the moment we apply it
                if (moment == effect.EffectMoment) {
                    Opponent = effect.Apply(this, belongsToPlayer);
                }
            }
            // for each effect from the item
            foreach (Effect effect in Opponent.Out.HeldItem.Effects) {
                // if the effect matches the moment we apply it
                if (moment == effect.EffectMoment) {
                    Opponent = effect.Apply(this, belongsToPlayer);
                }
            }
            // for each effect from the move
            foreach (Effect effect in Opponent.QueuedMove.Effects) {
                // if the effect matches the moment we apply it
                if (moment == effect.EffectMoment) {
                    Opponent = effect.Apply(this, belongsToPlayer);
                }
            }
            // for each effect from the statuses
            foreach (Status status in Opponent.AllStatus) {
                if (status is EffectStatus) {
                    Effect effect = ((EffectStatus)status).StatusEffect;
                    if (moment == effect.EffectMoment) {
                        Opponent = effect.Apply(this, belongsToPlayer);
                    }
                }
            }
        }
    }

    /*
     * Finds the damage multiplier for a move performed based on all the active statuses
     */
    private double StatusDamageMultiplier(DamagingTechnique technique, bool playerAttacking) {
        double multiplier = 1;
        if (playerAttacking) {
            // the damage boosts provided by the player status
            foreach (Status status in Player.PLAYER.AllStatus) {
                // if the status is a status affecting the attacking side of the move
                if (status is StatsStatus && ((StatsStatus) status).Stat == technique.AttackingStat) {
                    multiplier *= ((StatsStatus) status).Multiplier;
                }
            }
            // the damage boosts provided by the opponent status
            foreach (Status status in Opponent.AllStatus) {
                // if the status is a status affecting the defending side of the move
                if (status is StatsStatus && ((StatsStatus) status).Stat == technique.DefendingStat) {
                    multiplier /= ((StatsStatus) status).Multiplier;
                }
            }
        }
        else {
            // the damage boosts provided by the opponent status
            foreach (Status status in Opponent.AllStatus) {
                // if the status is a status affecting the attacking side of the move
                if (status is StatsStatus && ((StatsStatus) status).Stat == technique.AttackingStat) {
                    multiplier *= ((StatsStatus) status).Multiplier;
                }
            }
            // the damage boosts provided by the player status
            foreach (Status status in Player.PLAYER.AllStatus) {
                // if the status is a status affecting the defending side of the move
                if (status is StatsStatus && ((StatsStatus) status).Stat == technique.DefendingStat) {
                    multiplier /= ((StatsStatus) status).Multiplier;
                }
            }
        }
        return multiplier;
    }
}
