﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Class for all the tiles
 */
public abstract class Tile : OverworldObject {
   
    public static Tile GRASS = new Grass();
    public static Tile PATH = new Path();
}

/*
 * Class for a grass tile
 */
public class Grass : Tile {

}

/*
 * Class for a path tile
 */
public class Path : Tile {

}