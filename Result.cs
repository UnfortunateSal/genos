﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

/*
 * This class represents a result of an effect
 */
public abstract class Result {

    // true if the result changes is applied to same team the activateable belongs to, false otherwise
    public bool AppliedToOwner {get;}

    public Result(bool appliedToOwner) {
        this.AppliedToOwner = appliedToOwner;
    }

    /*
     * Applies the result to the field and returns it
     */
    public abstract Field Apply(Field field);

}

/*
 * This interface represents a result that adds a status
 */
public abstract class StatusResult : Result {

    // the status
    protected Status status;

    public StatusResult(Status status, bool appliedToOwner) : base(appliedToOwner) {
        this.status = status;
    }
}

/*
 * This class represents a result that adds a status to the out Genos
 */
public class GenosStatusResult : StatusResult {

    public GenosStatusResult(Status status, bool appliedToOwner) : base(status, appliedToOwner) {}

    /*
     * Adds a status to a the out Genos
     */
    public override Field Apply(Field field) {
        List<Status> currentStatus = field.Out.GenosStatus;
        if (!field.Out.GenosStatus.Contains(status)) {
            field.Out.GenosStatus.Add(status);
        }
        return field;
    }
}

/*
 * This class represents a result that adds a status to the Field
 */
public class FieldStatusResult : StatusResult {

    public FieldStatusResult(Status status, bool appliedToOwner) : base(status, appliedToOwner) {}

    /*
     * Adds a status to a the field
     */
    public override Field Apply(Field field) {
        if (!field.FieldStatus.Contains(status)) {
            field.FieldStatus.Add(status);
        }
        return field;
    }
}

/*
 * This class represents a result that removes all status of a type from a field
 */
public class RemoveStatusResult<T> : Result {

    public RemoveStatusResult(bool appliedToOwner) : base(appliedToOwner) { }

    /*
     * Removes all statuses of that type
     */
    public override Field Apply(Field field) {
        foreach (Status status in field.FieldStatus){
            if (status is T) {
                field.FieldStatus.Remove(status);
            }
        }
        foreach (Status status in field.Out.GenosStatus){
            if (status is T) {
                field.Out.GenosStatus.Remove(status);
            }
        }
        return field;
    }
}


/*
 * This class is a necessary special case to add the Protected status result
 */
public class ProtectedResult : Result {

    public ProtectedResult() : base(true) {}

    /*
     * We need to know the field to know in what position of the team the Protector is in
     */
    public override Field Apply(Field field) {
        if (field.QueuedMove is Switch) {
            return new FieldStatusResult(new Protected(((Switch)field.QueuedMove).Index), true).Apply(field);
        }
        else {
            throw new Exception("Should never happen");
        }
    }
}

/*
 * This class represents a result that changes the stats of the out Genos
 */
public abstract class StatResult : Result {

    // the stat in question and its stage
    private string stat;
    private int stage;

    public StatResult(string stat, int stage, bool appliedToOwner) : base(appliedToOwner) {
        this.stat = stat;
        this.stage = stage;
    }

    /* 
     * Converts the Result to a stat change
     */
    public override Field Apply(Field field) {
        field.Out.GenosStats.Modify(stat, stage);
        return field;
    }

}

/*
 * This class represents a result that changes the health of the out Genos by a certain percentage
 */
public class HealthResult : Result {

    // the percentage of max HP that is changed (negative if lost, positive if gained)
    private double percentage;

    public HealthResult(double percentage, bool appliedToOwner) : base(appliedToOwner) {
        this.percentage = percentage;
    }

    /* 
     * Converts the Result to an HP change and returns the resulting HP
     */
    public override Field Apply(Field field) {
        field.Out.CurrentHealth += Convert.ToInt32(field.Out.GenosStats.H.Value * percentage / 100);
        return field;
    }
}

/*
 * This class represents a result that deals damage to a Genos
 */
public class DamageResult : Result {

    // the attacking genos
    private Genos attacker;
    // the attack
    private DamagingTechnique attack;
    // the damage multiplier
    private double multiplier;

    public DamageResult(Genos attacker, DamagingTechnique attack, double multiplier, bool appliedToOwner) : base(appliedToOwner) {
        this.attacker = attacker;
        this.attack = attack;
        this.multiplier = multiplier;
    }

    /* 
     * Converts the Result to an HP change and returns the resulting HP
     */
    public override Field Apply(Field field) {
        // we get the Protected statuses
        List<Protected> statuses = field.AllStatus.FindAll(status => status is Protected).Select(status => (Protected) status).ToList();
        int damage = 0;
        // we calculate damage based on the attacking side
        // we reduce damage based on the Protected statuses
        if (attack is MeeleeTechnique) {
            damage = Convert.ToInt32(attack.Power * attacker.GenosStats.MS.Value / field.Out.GenosStats.MR.Value * attacker.Level / 150 * multiplier / (statuses.Count / 10));
        }
        if (attack is RangedTechnique) {
            damage = Convert.ToInt32(attack.Power * attacker.GenosStats.RS.Value / field.Out.GenosStats.RR.Value * attacker.Level / 150 * multiplier / (statuses.Count / 10));
        }
        field.Out.CurrentHealth = field.Out.CurrentHealth - damage;
        // for every protected status, we deal 10% of the normal damage
        foreach (Protected status in statuses) {
            if (attack is MeeleeTechnique) {
                damage = Convert.ToInt32(attack.Power * attacker.GenosStats.MS.Value / field.Team[status.Index].GenosStats.MR.Value * attacker.Level / 150 * multiplier / 10);
            }
            if (attack is RangedTechnique) {
                damage = Convert.ToInt32(attack.Power * attacker.GenosStats.RS.Value / field.Team[status.Index].GenosStats.RR.Value * attacker.Level / 150 * multiplier / 10);
            }
            field.Team[status.Index].CurrentHealth = field.Team[status.Index].CurrentHealth - damage;
        }
        return field;
    }
}

/*
 * This class represents a result that expends stamina from a Genos
 */
public class StaminaResult : Result {

    // the stamina amount that is changed (negative if lost, positive if gained)
    private int amount;

    public StaminaResult(int amount, bool appliedToOwner) : base(appliedToOwner) {
        this.amount = amount;
    }

    /* 
     * Converts the Result to an HP change and returns the resulting HP
     */
    public override Field Apply(Field field) {
        // finds the multiplier based on all active statuses
        double multiplier = 1;
        foreach (Status status in Player.PLAYER.AllStatus) {
            // if the status is a status affecting the stamina of the owner
            if (status is StaminaRegenerationStatus) {
                multiplier *= ((StaminaRegenerationStatus)status).Multiplier;
            }
        }
        field.Out.Stamina += Convert.ToInt32(amount * multiplier);
        return field;
    }
}

/*
 * This class represents a result that changes the class of a Genos
 */
public class ClassResult : Result {

    // the new classes
    private List<GenosClass> newClasses;
    // true if the first class of the Genos is replaced

    public ClassResult(List<GenosClass> newClasses, bool appliedToOwner) : base(appliedToOwner) {
        this.newClasses = newClasses;
    }

    /* 
     * Converts the Result to a new classification
     */
    public override Field Apply(Field field) {
        field.Out.Classes = newClasses;
        return field;
    }
}

/*
 * This class represents a result that changes the ability of a Genos
 */
public class AbilityResult : Result {

    // the new classes
    private Ability newAbility;
    // true if the first class of the Genos is replaced

    public AbilityResult(Ability newAbility, bool appliedToOwner) : base(appliedToOwner) {
        this.newAbility = newAbility;
    }

    /* 
     * Converts the Result to a new classification
     */
    public override Field Apply(Field field) {
        field.Out.GenosAbility = newAbility;
        return field;
    }
}
