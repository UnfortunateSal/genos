﻿using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;

/*
 * This class represents the player character
 */
public class Player : MonoBehaviour, Field {

    // There is only one player so we can instantiate them
    public static Player PLAYER = new Player();

    // The amount of time the player cna wiat after selecting a move in ms
    private static int WAIT_TIME = 1000;

    // The position of the player and its zone
    private Position position;
    private Zone zone;

    // If we are waiting for an input
    private bool waitForInput;

    // the orientation of the player
    private Direction orientation;

    // The player's name and pronouns
    public string Name {get; set;}
    public List<PronounSet> Pronouns {get; set;}

    // The current menu
    public Menu OpenMenu {get; set;}

    // All the Genos in storage
    public List<Genos> Storage {get; private set;}

    // All the items and their quantity
    public Dictionary<Item, int> Bag {get; private set;}

    /*
     * Initializes the player
     */
    private Player() {
        position = Position.DEFAULT;
        zone = new Home();
        Team = new List<Genos>();
        Storage = new List<Genos>();
        Bag = new Dictionary<Item, int>();
        OpenMenu = null;
        FieldStatus = new List<Status>();
        QueuedMove = null;
    }

    /*-------------------------------------------------------------------------------------------------------------------------
     All the MonoBehaviour functions
    -------------------------------------------------------------------------------------------------------------------------*/

    public void Start() {

    }

    /*
     * If there is no input, gets an input every frame
     */
    public void Update() {
        // if we are waiting for an input
        if (waitForInput) {
            waitForInput = false;
            if (Input.GetButton("Up")) {
                Up();
            }
            else if (Input.GetButtonDown("Right")) {
                Right();
            }
            else if (Input.GetButtonDown("Down")) {
                Down();
            }
            else if (Input.GetButtonDown("Left")) {
                Left();
            }
            else if (Input.GetButtonDown("Select")) {
                Select();
            }
            else if (Input.GetButtonDown("Cancel")) {
                Cancel();
            }
            if (Input.GetButtonDown("Menu")) {
                Menu();
            }
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------
     All the field properties and functions
    -------------------------------------------------------------------------------------------------------------------------*/

    public List<Status> FieldStatus {get; set;}

    public List<Status> AllStatus {get{return Out.GenosStatus.Concat(FieldStatus).ToList(); }}

    public Move QueuedMove {get; set;}

    // the player is always ready for battle (this should never be called)
    public bool ReadyForBattle {get{return true;}}

    public List<Genos> Team {get; set;}

    public Genos Out {get{return Team[0];} set{Team[0] = value;}}

    public bool HasLost {get{return Team.TrueForAll(Genos => Genos.CurrentHealth == 0);}}

    public int PriorityBracket {get{
            int bracket = 0;
            foreach (Status status in AllStatus){
                // if the status is a status affecting speed
                if (status is PriorityStatus){
                    bracket += ((PriorityStatus) status).PriorityBracket;
                }
            }
            return bracket;}}

    public void WaitForMove() {
        // while there is no move, we get the next input and wait for 1 second
        // we loop endlessly
        while (true) {
            // if a move is selected
            if (QueuedMove != null) {
                // we save the move
                Move move = QueuedMove;
                // we wait one second
                Thread.Sleep(WAIT_TIME);
                // if the move is the same after one second
                if (move == QueuedMove) {
                    // we break the loop
                    break;
                }
            }
        }
        // we go to the main battle menu
        OpenMenu.Cancel();
    }

    /*-------------------------------------------------------------------------------------------------------------------------
     All the commands
    -------------------------------------------------------------------------------------------------------------------------*/

    /*
     * UP command
     */
    private void Up(){
        if (OpenMenu == null) {
            Move(Direction.UP);
        }
        else {
            OpenMenu.Up();
        }
    }

    /*
     * RIGHT command
     */
    private void Right(){
        if (OpenMenu == null) {
            Move(Direction.RIGHT);
        }
        else {
            OpenMenu.Right();
        }
    }

    /*
     * DOWN command
     */
    private void Down() {
        if (OpenMenu == null) {
            Move(Direction.DOWN);
        }
        else {
            OpenMenu.Down();
        }
    }

    /*
     * LEFT command
     */
    private void Left() {
        if (OpenMenu == null) {
            Move(Direction.LEFT);
        }
        else {
            OpenMenu.Left();
        }
    }

    /*
     * SELECT command
     */
    private void Select() {
        if (OpenMenu == null){
            Interact();
        }
        else {
            OpenMenu.Select();
        }
    }

    /*
     * CANCEL command
     */
    private void Cancel(){
        if (OpenMenu != null && OpenMenu is CancelableMenu){
            OpenMenu = new MainMenu();
        }
    }

    /*
     * MENU command
     */
    private void Menu() {
        if (OpenMenu == null) {
            OpenMenu = new MainMenu();
        }
        else if (OpenMenu is MainMenu) {
            OpenMenu = null;
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------
     Private functions for overworld
    -------------------------------------------------------------------------------------------------------------------------*/

    /*
     * The player moves in a given direction
     */
    private void Move(Direction direction) {
        if (zone.InFront(position, direction) == null) {
            orientation = direction;
            position.Move(direction);
            TryWarp(zone.TileAt(position));
        }
    }

    /*
     * The player interacts with the obstacle in front of it
     */
    private void Interact() {
        Obstacle obstacle = zone.InFront(position, orientation);
        if (obstacle is Interactable && ((Interactable) obstacle).IsInteractableFrom(orientation)) {
            ((Interactable) obstacle).Activate();
            TryWarp(obstacle);
            TryBattle(obstacle);
        }
    }

    /*
     * This function checks if we can warp the player, and if so, does it.
     */
    private void TryWarp(OverworldObject overworldObject) {
        if (overworldObject is Warp) {
            Warp warp = (Warp) overworldObject;
            zone = warp.TargetZone();
            position = warp.TargetPosition();
        }
    }

    /*
     * This function checks if we can make the player battle, and if so, does it
     */
    private void TryBattle(OverworldObject overworldObject) {
        if (overworldObject is Field && ((Field) overworldObject).ReadyForBattle) {
            Field opponent = ((Field) overworldObject);
            if (opponent is WildBattleTile) {
                OpenMenu = new WildBattleMenu();
            }
            if (opponent is Trainer) {
                OpenMenu = new TrainerBattleMenu();
            }
            new Battle(opponent);
        }
    }
}




