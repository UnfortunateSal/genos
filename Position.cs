﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * This class defines the default position of an overworld object on the map
 */
public class Position {
    
    // the default position of the player character in the first zone
    public static Position DEFAULT = new Position(5, 5);

    public int X {get; private set;}
    public int Y {get; private set;}

    public Position(int x, int y) {
        this.X = x;
        this.Y = y;
    }

    /*
     * Moves the position in a given direction
     */
    public void Move(Direction direction) {
        switch (direction) {
            case (Direction.UP):
                Y--;
                break;
            case (Direction.RIGHT):
                X++;
                break;
            case (Direction.DOWN):
                Y++;
                break;
            case (Direction.LEFT):
                X--;
                break;
            default: break;
        }
    }

    /*
     * Moves the position in a given direction, while also looping based on the rows and columns
     */
    public void MoveLooping(Direction direction, int x, int y) {
        Move(direction);
        X %= x;
        Y %= y;
    }
}

/*
 * This enum defines teh direction an object is facing or moving in the map
 */
public enum Direction {
    UP, RIGHT, DOWN, LEFT
}

// extension class for directions
public static class DirectionExtension {

    /*
     * Checks if the two directions are opposite from each other, returns true if they are and false otherwise
     */
    public static bool AreOpposites(Direction dir1, Direction dir2) {
        return (dir1 == Direction.UP && dir2 == Direction.DOWN)
            || (dir1 == Direction.RIGHT && dir2 == Direction.LEFT)
            || (dir1 == Direction.DOWN && dir2 == Direction.UP)
            || (dir1 == Direction.LEFT && dir2 == Direction.RIGHT);
    }
}
