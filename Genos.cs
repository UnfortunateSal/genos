﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using UnityEngine;

/*
 * This class represents a Genos
 */
public class Genos {
    // the stamina gained every turn
    private static int STAMINA_GAINED_PER_TURN = 10;

    // true if this Genos uses its first ability, false otherwise
    private bool firstAbility;
    // the SPs of the Genos
    private int[] sps;

    // the list of statuses that affect the Genos
    public List<Status> GenosStatus {get; private set;}
    // the name of the Genos
    public string Name {get; set;}
    // the species of the Genos
    public Species GenosSpecies {get; private set;}
    // the current level of the Genos
    public int Level {get; private set;}
    // the current EXP of the Genos
    public int Exp {get; private set;}
    // the current moveset of the Genos
    public List<Technique> Moveset {get; private set;}
    // the movepool of the Genos
    public List<Technique> Movepool {get; private set;}
    // The classification of the Genos
    public List<GenosClass> Classes {get; set;}
    // the ability of the Genos
    public Ability GenosAbility {get; set;}
    // the stats of the Genos
    public Stats GenosStats {get; private set;}
    // the held item of the Genos
    public Item HeldItem {get; set;}
    // the current of the Genos
    public int CurrentHealth {get; set;}
    // the current stamina of the Genos
    public int Stamina {get; set;}

    public Genos(Species species, int level, bool firstAbility, List<Technique> moveset) {
        this.Name = species.Name;
        this.GenosSpecies = species;
        this.Level = level;
        this.firstAbility = firstAbility;
        this.Movepool = GetMovepool();
        this.Moveset = moveset;
        this.Classes = species.Classes;
        this.GenosAbility = species.GetAbility(firstAbility);
        this.GenosStats = new Stats(species, level);
        this.sps = new int[] {0, 0, 0, 0, 0, 0};
        this.HeldItem = Holdable.NONE;
        this.Exp = (Level - 1) * 100;
        // every Genos can regenrate stamina
        this.GenosStatus = new List<Status>(){
            new EffectStatus(new Effect(Moment.END_OF_TURN)
                             .AddResult(new StaminaResult(STAMINA_GAINED_PER_TURN, true)),
                             -1,
                             false)
        };
        this.CurrentHealth = GenosStats.H.BaseValue;
        this.Stamina = 100;
    }

    /*
     * Returns the STAB multiplier of the move
     */
    public double STAB(Technique move) {
        if (move.TechniqueClass == Classes.First()) {
            return 5/3;
        }
        else if (move.TechniqueClass == Classes.Last()) {
            return 4/3;
        }
        else {
            return 1;
        }
    }

    /*
     * Adds one SP to the stat of choice if possible
     */
    public void addSP(string stat) {
        if (sps.Sum() < Level) {
            switch (stat) {
                case "H":
                    if (sps[0] < 50) {
                        GenosStats.IncreaseH();
                        sps[0]++;
                    }
                    else {
                        throw new Exception("Health SPs already maxed out!");
                    }
                    break;
                case "MS":
                    if (sps[1] < 50) {
                        GenosStats.IncreaseMS();
                        sps[1]++;
                    }
                    else {
                        throw new Exception("Meelee Strength SPs already maxed out!");
                    }
                    break;
                case "MR":
                    if (sps[2] < 50) {
                        GenosStats.IncreaseMR();
                        sps[2]++;
                    }
                    else {
                        throw new Exception("Meelee Resilience SPs already maxed out!");
                    }
                    break;
                case "RS":
                    if (sps[3] < 50)
                    {
                        GenosStats.IncreaseRS();
                        sps[3]++;
                    }
                    else {
                        throw new Exception("Ranged Strength SPs already maxed out!");
                    }
                    break;
                case "RR":
                    if (sps[4] < 50) {
                        GenosStats.IncreaseRR();
                        sps[4]++;
                    }
                    else {
                        throw new Exception("Ranged Resilience SPs already maxed out!");
                    }
                    break;
                case "RT":
                    if (sps[5] < 50) {
                        GenosStats.IncreaseRT();
                        sps[5]++;
                    }
                    else {
                        throw new Exception("Reaction Time SPs already maxed out!");
                    }
                    break;
                default:
                    throw new Exception("Invalid stat!");
            }
        }
        else {
            throw new Exception("SPs already maxed out!");
        }
    }

    /*
     * Gives this Genos experience from defeating the given Genos
     */
     public void GainExp(Genos genos)
    {
        int experience = 0;
        // TODO : Zoen this is your job right
        Exp += experience;
        Exp %= 99000;
        while (experience / 100 > Level - 1) {
            LevelUp();
        }
    }

    /*
     * Levels the Genos up
     */
    public void LevelUp() {
        GenosStats = new Stats(GenosSpecies, Level, sps);
        this.Movepool = GetMovepool();
    }

    /*
     * Evolves the Genos into the given species
     */
    public void Evolve(Species evolution) {
        GenosSpecies = evolution;
        GenosAbility = evolution.GetAbility(firstAbility);
        GenosStats = new Stats(evolution, Level, sps);
    }

    /*
     * Applies the given result to the Genos
     */
    public void Apply(Effect effect) {
        // TODO
    }

    /*
     * Returns the Genos's movepool up to the level it is at
     */
    private List<Technique> GetMovepool() {
        return new List<Technique>(GenosSpecies.Movepool.Where(entry => (entry.Value <= Level)).Select(entry => entry.Key));
    }
}

/*
 * This class represents all the stats of a Genos
 */
public class Stats {

    // The Health stat
    public Stat H {get; private set;}
    // The Meelee Strength stat
    public Stat MS {get; private set;}
    // The Meelee Resilience stat
    public Stat MR {get; private set;}
    // The Ranged Strength stat
    public Stat RS {get; private set;}
    // The Ranged Resilience stat
    public Stat RR {get; private set;}
    // The Reaction Time stat
    public Stat RT {get; private set;}

    /*
     * Initializes the stats of a species at a given level with no sps, based on the base stats of the species
     */
    public Stats(Species species, int level) {
        H = new Stat("H", "H", Convert.ToInt32(level * Ratio(species.SpeciesBaseStats.H)));
        MS = new Stat("MS", "Meelee Strength", Convert.ToInt32(level * Ratio(species.SpeciesBaseStats.MS)));
        MR = new Stat("MR", "Meelee Resilience", Convert.ToInt32(level * Ratio(species.SpeciesBaseStats.MR)));
        RS = new Stat("RS", "Ranged Strength", Convert.ToInt32(level * Ratio(species.SpeciesBaseStats.RS)));
        RR = new Stat("RR", "Ranged Resilience", Convert.ToInt32(level * Ratio(species.SpeciesBaseStats.RR)));
        RT = new Stat("RT", "Reaction Time", Convert.ToInt32(level * Ratio(species.SpeciesBaseStats.RT)));
    }

    /*
     * Initialized the stats of a species at a given level with given sps
     */
    public Stats(Species species, int level, int[] sps) : this(species, level) {
        H.Add(sps[0]);
        MS.Add(sps[1]);
        MR.Add(sps[2]);
        RS.Add(sps[3]);
        RR.Add(sps[4]);
        RT.Add(sps[5]);
    }

    /*
     * Increases Health by 1
     */
    public void IncreaseH() {
        H.Increment();
    }

    /*
     * Increases Meelee Strength by 1
     */
    public void IncreaseMS() {
        MS.Increment();
    }

    /*
     * Increases Meelee Resilience by 1
     */
    public void IncreaseMR() {
        MR.Increment();
    }

    /*
    * Increases Ranged Strength by 1
    */
    public void IncreaseRS() {
        RS.Increment();
    }

    /*
    * Increases Ranged Resilience by 1
    */
    public void IncreaseRR() {
        RR.Increment();
    }

    /*
    * Increases Reaction Time by 1
    */
    public void IncreaseRT() {
        RT.Increment();
    }

    /*
     * Return the stats after the multiplier has been modified by the given amount of stages
     */
    public void Modify(string stat, int stages) {
        switch (stat) {
            case "H": 
                H.MultiplierStages += stages;
                H.MultiplierStages %= 5;
                break;
            case "MS":
                MS.MultiplierStages += stages;
                MS.MultiplierStages %= 5; 
                break;
            case "MR":
                MR.MultiplierStages += stages;
                MR.MultiplierStages %= 5;
                break;
            case "RS":
                RS.MultiplierStages += stages;
                RS.MultiplierStages %= 5;
                break;
            case "RR":
                RR.MultiplierStages += stages;
                RR.MultiplierStages %= 5;
                break;
            case "RT":
                RT.MultiplierStages += stages;
                RT.MultiplierStages %= 5;
                break;
        }
    }

    /*
     * Returns the ratio factor of the stat (useful for damage calucations)
     */
    private double Ratio(int stat) {
        return 1 + Math.Pow(stat / 100.0, 2);
    }
}

/*
 * Class for a singluar stat
 */
public class Stat {

    // Name of the stat
    public string AbbreviatedName {get;}
    public string FullName {get;}
    // Base value of the stat
    public int BaseValue {get; private set;}
    // Stages for the stat multiplier
    public int MultiplierStages {get; set;}
    // The true value of the stat
    public int Value {get{return MultiplierStages > 0 ? BaseValue + BaseValue * MultiplierStages / 2 : BaseValue * 2 / (2 - MultiplierStages);}}

    public Stat(String abbreviatedName, String fullName, int value) {
        this.AbbreviatedName = abbreviatedName;
        this.FullName = fullName;
        this.BaseValue = value;
        this.MultiplierStages = 0;
    }

    /*
     * Increments the stat
     */
    public void Increment() {
        BaseValue++;
    }

    /*
     * Adds a value to the stat
     */
    public void Add(int value) {
        BaseValue += value;
    }
}