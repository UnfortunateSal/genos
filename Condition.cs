﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.NetworkInformation;

/*
 * This class represents a condition that needs to be met for an effect to occur
 */
public abstract class Condition {

    // true if the condition is checked on the owner, false otherwise
    public bool CheckOnOwner {get;}

    public Condition(bool checkOnOwner) {
        this.CheckOnOwner = checkOnOwner;
    }

    public abstract bool Check(Field field);
}

/*
 * This class represents a condition that a move of a certain type is used by the field
 */
public class MoveTypeCondition<T> : Condition {

    public MoveTypeCondition(bool checkOnOwner) : base(checkOnOwner) {}

    public override bool Check(Field field) {
        return field.QueuedMove is T;
    }
}

/*
 * This class represents a condition that a certain status is present on an out Genos or not
 */
public class StatusCondition<T> : Condition {

    // true if the status has to be present, false if it has to not be present
    private bool checkIfPresent;

    public StatusCondition(bool checkIfPresent, bool checkOnOwner) : base(checkOnOwner) {
        this.checkIfPresent = checkIfPresent;
    }

     public override bool Check(Field field) {
        bool absent = true;
        // for each status, we check if it's absent
        foreach(Status status in field.AllStatus) {
            if(status is T) {
                absent = false;
                break;
            }
        }
        // if we check if it's active, we return if it's not absent, otherwise we return if it's absent
        return checkIfPresent ^ absent;
    }
}

/*
 * This interface represents a condition that the out Genos has a certain stat over or under a certain value
 * Can be: stamina less than 50%, attack over a certain value, HP is full
 */
public class StatCondition : Condition {

    public StatCondition(bool checkOnOwner) : base(checkOnOwner)
    {
        // TODO
    }

    public override bool Check(Field field)
    {
        // TODO
        return true;
    }
}

/*
 * This interface represents a condition that the out Genos is of a certain class
 */
public class ClassCondition : Condition {

    // the class that is checked for
    private GenosClass genosClass;
    // true if the class has to be present, false if it has to not be present
    private bool checkIfPresent;

    public ClassCondition(GenosClass genosClass, bool checkIfPresent, bool checkOnOwner) : base(checkOnOwner) {
        this.checkIfPresent = checkIfPresent;
        this.genosClass = genosClass;
    }

    public override bool Check(Field field) {
        // if we check if the class is present, we return if it's present, otherwise we return if it's absent
        return field.Out.Classes.Contains(genosClass) ^ !checkIfPresent;
    }
}


/*
 * This class represents that a certain probability chance is hit
 */
public class ChanceCondition : Condition {

    // the probability that the effect will occur
    private double probability;

    public ChanceCondition(double probability) : base(true) {
        this.probability = probability;
    }

    /*
     * Rolls a probaility chance
     */
    public override bool Check(Field field) {
        Random random = new Random();
        return random.NextDouble() < probability;
    }
}

