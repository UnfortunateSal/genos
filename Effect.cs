﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

/*
 * This class represents the effect of an activateable
 */
public class Effect {

    // The conditions that are needed on the field to activate the effect
    private List<Condition> conditions;

    // The results of the effect
    private List<Result> results;

    // true if the effect is applied to

    // The moment at which the effect can activate
    public Moment EffectMoment {get;}

    // initializes an effect that occurs at a given moment
    public Effect(Moment moment) {
        this.EffectMoment = moment;
        this.conditions = new List<Condition>();
        this.results = new List<Result>();
    }

    /*
     * This function adds a condition to the effect
     */
    public Effect AddCondition(Condition condition) {
        conditions.Add(condition);
        return this;
    }

    /*
     * This function adds a result to the effect
     */
    public Effect AddResult(Result result) {
        results.Add(result);
        return this;
    }

    /* 
     * This function returns the opponent after the effect has been applied
     * The first boolean indicates if the activateable with this effect effect belongs to the player or not
     */
    public Field Apply(Battle battle, bool belongsToPlayer) {
        // if the conditions are all true
        if (AllConditionsTrue(battle, belongsToPlayer)) {
            return ApplyAllResults(battle, belongsToPlayer);
        }
        else {
            return battle.Opponent;
        }
    }

    /*
     * Returns true if all the conditions are true
     */
    private bool AllConditionsTrue(Battle battle, bool belongsToPlayer) {
        // first, we perform all the checks
        bool check = true;
        foreach (Condition condition in conditions) {
            if (condition.CheckOnOwner ^ belongsToPlayer) {
                // if the condition is checked on the owner and the opponent is the player
                // OR if the condition is checked on the opponent and the owner is the player, the condition is checked on the opponent
                check = check && condition.Check(battle.Opponent);
            }
            else {
                // if the condition is checked on the owner and the owner is the player
                // OR if the condition is checked on the opponent and the owner is the opponent, the condition is checked on the player
                check = check && condition.Check(Player.PLAYER);
            }
        }
        return check;
    }

    private Field ApplyAllResults(Battle battle, bool belongsToPlayer) {
        foreach (Result result in results) {
            if (result.AppliedToOwner ^ belongsToPlayer) {
                // if the result is applied to the owner and the opponent is the player, the condition is applied to the opponent
                // OR if the result is applied to the opponent and the owner is the player, the condition is applied to the opponent
                result.Apply(battle.Opponent);
            }
            else {
                // if the result is applied to the owner and the owner is the player, the condition is applied to the player
                // OR if the result is applied to the opponent and the opponent is the player, the condition is applied to the player
                result.Apply(Player.PLAYER);
            }
        }
        // we return the opponent (the player is already updated)
        return battle.Opponent;
    }
}

/*
* This enum represents the moment the effect can activate
*/
public enum Moment {
    START_OF_TURN, AFTER_MOVE, END_OF_TURN
}