﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

/*
 * This file contains every class that represents a menu
 */

/*
 * The base Menu class, from which all menus are derived
 */
public abstract class Menu {

    // the current selection
    public Position Cursor {get; private set;}

    // the number of rows
    private int rows;

    // the number of columns
    private int columns;

    protected Menu(Position cursor, int columns, int rows) {
        this.Cursor = cursor;
        this.columns = columns;
        this.rows = rows;
    }

    /*
    * The UP command, which moves the cursor up in the menu
    */
    public virtual void Up() {
        Cursor.MoveLooping(Direction.UP, columns, rows);
    }

    /*
    * The RIGHT command, which moves the cursor up in the menu
    */
    public virtual void Right() {
        Cursor.MoveLooping(Direction.RIGHT, columns, rows);
    }

    /*
    * The DOWN command, which moves the cursor right in the menu
    */
    public virtual void Down() {
        Cursor.MoveLooping(Direction.DOWN, columns, rows);
    }

    /*
    * The LEFT command, which moves the cursor left in the menu
    */
    public virtual void Left() {
        Cursor.MoveLooping(Direction.LEFT, columns, rows);
    }

    /*
    * The SELECT command, which confirms a selection, and performs an action based on that selection
    */
    public abstract void Select();

    /*
    * The CANCEL command, which does nothing by default
    */
    public virtual void Cancel() {}
}

/*
 * This is a menu that can be canceled in order to return to a previous menu
 * This applies to almost all menus.
 */
public abstract class CancelableMenu : Menu {

    // the previous menu
    protected Menu previousMenu;

    protected CancelableMenu(Menu menu, Position cursor, int columns, int rows) : base(cursor, columns, rows) {
        this.previousMenu = menu;
    }

    /*
    * The CANCEL command, which cancels the selection of this menu, and returns the the previous menu
    */
    public override void Cancel() {
        Player.PLAYER.OpenMenu = previousMenu;
    }
}

/*
 * The main menu, which is the menu you get when you press the Menu button in the overworld
 * This menu is cancelable, and when canceled, we return to having no open menu
 */
public class MainMenu : CancelableMenu {

    public MainMenu() : base(null, new Position(0, 0), 1, 3) { }

    public override void Select() {
        switch (Cursor.Y) {
            case 0:
                // we have selected the team menu
                // the player opens the team menu
                Player.PLAYER.OpenMenu = new TeamInfoMenu(this);
                break;
            case 1:
                // we have selected the map menu
                // the player opens the map menu
                Player.PLAYER.OpenMenu = new MapMenu(this);
                break;
            case 2:
                // we have selected the item menu
                // the player opens the item menu
                Player.PLAYER.OpenMenu = new ItemMenu(this);
                break;
            case 3:
                // we have selected the Genosdex
                // the player opens the Genosdex
                Player.PLAYER.OpenMenu = new Genosdex(this);
                break;
            case 4:
                // we have selected the player info Menu
                // the player opens the player info menu
                Player.PLAYER.OpenMenu = new PlayerInfoMenu(this);
                break;
            default: break;
        }
    }
}

/*
 * A menu that shows the team
 */
public abstract class TeamMenu : CancelableMenu {

    public TeamMenu(Menu menu) : base(menu, new Position(0, 0), 3, 2) { }

    /*
     * The UP command must account for the number of rows and columns based on the empty slots.
     */
    public override void Up() {
        Cursor.MoveLooping(Direction.UP, Columns(), Rows());
    }

    /*
     * The RIGHT command must account for the number of rows and columns based on the empty slots.
     */
    public override void Right() {
        Cursor.MoveLooping(Direction.RIGHT, Columns(), Rows());
    }

    /*
     * The UP command must account for the number of rows and columns based on the empty slots
     */
    public override void Down() {
        Cursor.MoveLooping(Direction.DOWN, Columns(), Rows());
    }

    /*
     * The LEFT command must account for the number of rows and columns based on the empty slots
     */
    public override void Left() {
        Cursor.MoveLooping(Direction.LEFT, Columns(), Rows());
    }

    /*
     * The number of columns based on the position and number of team members
     */
    private int Columns() {

        /*
        Cursor.Y = 0;
        1 -> 1
        2 -> 2
        3 -> 3
        4 -> 3
        5 -> 3
        6 -> 3
        Cursor.Y = 1;
        1-> x
        2-> x
        3-> x
        4-> 1
        5-> 2
        6-> 3
        */

        return Math.Min(3, Player.PLAYER.Team.Count - Cursor.Y * 3);
    }

    /*
     * The number of columns based on the position and number of team members
     */
    private int Rows() {
        /*
        Cursor.X = 0;
        1 -> 1
        2 -> 1
        3 -> 1
        4 -> 2
        5 -> 2
        6 -> 2
        Cursor.X = 1;
        1-> x
        2-> 1
        3-> 1
        4-> 1
        5-> 2
        6-> 2
        Cursor.X = 2;
        1-> x
        2-> x
        3-> 1
        4-> 1
        5-> 1
        6-> 2
        */

        return Math.Min(2, (Player.PLAYER.Team.Count + 2 - Cursor.X) / 3);
    }
}

/*
 * A menu that shows the team ouside of battle
 */
public class TeamInfoMenu : TeamMenu {

    public TeamInfoMenu(Menu menu) : base(menu) { }

    public override void Select() {
        Player.PLAYER.OpenMenu = new GenosMenu(this, Player.PLAYER.Team[Cursor.X + 3 * Cursor.Y]);
    }
}

/*
 * A menu that shows the data of an individual Genos, acessible from the team info menu
 */
public class GenosMenu : CancelableMenu {

    private Genos genos;

    public GenosMenu(Menu menu, Genos genos) : base(menu, new Position(0, 0), 1, 1) {
        this.genos = genos;
    }

    public override void Select() {
        // TODO
    }

}

/*
 * A menu that shows the Genos in storage
 */
public class StorageMenu : CancelableMenu {

    public StorageMenu(Menu menu) : base(menu, new Position(0, 0), 1, Player.PLAYER.Storage.Count) { } // TODO

    public override void Select() {
        // TODO
    }
}

/*
 * A menu that shows the map
 */
public class MapMenu : CancelableMenu
{
    public MapMenu(Menu menu) : base(menu, new Position(0, 0), 10, 10) { } // TODO

    public override void Select() {
        // baed on the position, warps to the zone
    }

}

/*
 * A menu that shows the items in storage
 */
public class ItemMenu : CancelableMenu {

    public ItemMenu(Menu menu) : base(menu, new Position(0, 0), 1, Player.PLAYER.Bag.Count) { } // TODO

    public override void Select() {
        // TODO
    }

}

/*
 * A menu that shows the Genosdex
 */
public class Genosdex : CancelableMenu {

    /*
     * the list of all Genos
     */
    private static List<Species> dex = new List<Species>();

    // fills the Genosdex
    private static void FillDex() {
        foreach (FieldInfo info in typeof(Species).GetFields())
        {
            if (info.GetRawConstantValue() is Species)
            {
                dex.Add((Species)info.GetRawConstantValue());
            }
        }
    }

    public Genosdex(Menu menu) : base(menu, new Position(0, 0), 1, 100) { } // TODO

    public override void Select() {
        // TODO
    }
}

/*
 * A menu that shows the player info
 */
public class PlayerInfoMenu : CancelableMenu {

    public PlayerInfoMenu(Menu menu) : base(menu, new Position(0, 0), 1, 2) { } // TODO

    public override void Select()
    {
        // TODO
    }

}

/*
 * A menu that shows the info for an individual species
 */
public class SpeciesMenu : CancelableMenu {

    private Species species;

    public SpeciesMenu(Menu menu, Species species) : base(menu, new Position(0, 0), 1, 1) {
        this.species = species;
    } // TODO

    public override void Select() {
        // TODO
    }
}

/*
 * The menu that appears when battling a wild Genos
 */
public class WildBattleMenu : Menu {

    public WildBattleMenu() : base(new Position(2, 0), 5, 1) { }

    public override void Select() {
        switch (Cursor.X) {
            case 0:
                // we have selected "run"
                // the player runs
                Player.PLAYER.OpenMenu = new MoveConfirmationMenu(this, Move.RUN);
                break;
            case 1:
                // we have selected "fight"
                // the player opens the fight menu
                Player.PLAYER.OpenMenu = new FightMenu(this);
                break;
            case 2:
                // we have selected "Technique"
                // the player opens the technique menu
                Player.PLAYER.OpenMenu = new TechniqueMenu(this);
                break;
            case 3:
                // we have selected "switch"
                // the player opens the switch menu
                Player.PLAYER.OpenMenu = new SwitchMenu(this);
                break;
            case 4:
                // we have selected "scan"
                // the player catches the opposing wild Genos
                Player.PLAYER.OpenMenu = new MoveConfirmationMenu(this, Move.SCAN);
                break;
            default: break;
        }
    }
}

/*
 * The menu that appears when battling a trainer
 */
public class TrainerBattleMenu : Menu {

    public TrainerBattleMenu() : base(new Position(1, 0), 3, 1) { }

    public override void Select() {
        switch (Cursor.X) {
            case 0:
                // we have selected "fight"
                // the player opens the fight menu
                Player.PLAYER.OpenMenu = new FightMenu(this);
                break;
            case 1:
                // we have selected "technique"
                // the player opens the technique menu
                Player.PLAYER.OpenMenu = new TechniqueMenu(this);
                break;
            case 2:
                // we have selected "switch"
                // the player opens the switch menu
                Player.PLAYER.OpenMenu = new SwitchMenu(this);
                break;
            default: break;
        }
    }
}

/*
 * The menu that allows the trainer to select fight commands
 */
public class FightMenu : CancelableMenu {

    public FightMenu(Menu menu) : base(menu, new Position(0, 0), 4, 1) { }

    public override void Select() {
        switch (Cursor.X) {
            case 0:
                // we have selected "bash"
                // the player opens the cancel menu for a bash
                Player.PLAYER.OpenMenu = new MoveConfirmationMenu(previousMenu, Move.BASH);
                break;
            case 1:
                // we have selected "throw"
                // the player opens the confirmation menu for a throw
                Player.PLAYER.OpenMenu = new MoveConfirmationMenu(previousMenu, Move.THROW);
                break;
            case 2:
                // we have selected "guard"
                // the player opens the confirmation menu for a guard
                Player.PLAYER.OpenMenu = new MoveConfirmationMenu(previousMenu, Move.GUARD);
                break;
            case 3:
                // we have selected "rest"
                // the player opens the confirmation menu for a rest
                Player.PLAYER.OpenMenu = new MoveConfirmationMenu(previousMenu, Move.REST);
                break;
            default: break;
        }
    }
}

/*
 * The menu that allows the trainer to select techniques
 */
public class TechniqueMenu : CancelableMenu
{
    public TechniqueMenu(Menu menu) : base(menu, new Position(0, 0), 4, 1) { }

    public override void Select() {
        // the player opens the confirmation menu for the selected move
        Player.PLAYER.OpenMenu = new MoveConfirmationMenu(previousMenu, Player.PLAYER.Out.Moveset[Cursor.X]);
    }
}

/*
 * The menu that allows the trainer to switch
 */
public class SwitchMenu : TeamMenu {

    public SwitchMenu(Menu menu) : base(menu) {}

    public override void Select() {
        // the player opens the confirmation menu for a switch to the selected Genos
        Player.PLAYER.OpenMenu = new MoveConfirmationMenu(previousMenu, new Switch(Cursor.X + 3 * Cursor.Y));
    }
}

/*
 * This menu exists to give the player one last chance to cancel a move
 * If a certain amount of time passes, and the move is not yet canceled and the opponent has picked their move, said move willl go through
 */
public class MoveConfirmationMenu : CancelableMenu {

    public MoveConfirmationMenu(Menu menu, Move move) : base(menu, new Position(0, 0), 1, 1) {
        // When this menu is created, the move gets queued
        Player.PLAYER.QueuedMove = move;
    }

    /*
     * There is only one selection, which does the same thing as cancelling
     * As such, the SELECT button does the same thing as the CANCEL button
     */
    public override void Select() {
        Cancel();
    }

    /*
     * When the CANCEL button is pressed, we remove the queued move
     */
    public override void Cancel() {
        base.Cancel();
        Player.PLAYER.QueuedMove = null;
    }
}
