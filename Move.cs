﻿using System.Collections;
using System.Collections.Generic;

/*
 * This class represents a move
 */
public class Move : Activateable {

    // the 4 default moves

    public static Move BASH = new MeeleeTechnique("Bash", "", null, 30, 0);

    public static Move THROW = new RangedTechnique("Throw", "", null, 30, 0);

    public static Move GUARD = new SupportTechnique("Guard", "", null, 0)
    // Moment : Start of turn
        .AddEffect(new Effect(Moment.START_OF_TURN)
    // Result : +1 priority
            .AddResult(new GenosStatusResult(new PriorityStatus(1), true)))
    // Moment : After the move
        .AddEffect(new Effect(Moment.AFTER_MOVE)
    // Result : +1 MR and RR
            .AddResult(new GenosStatusResult(new StatsStatus("MR", 1.5), true))
            .AddResult(new GenosStatusResult(new StatsStatus("RR", 1.5), true)));

    public static Move REST = new SupportTechnique("Rest", "", null, 0)
    // Moment : After the move
        .AddEffect(new Effect(Moment.AFTER_MOVE)
    // Result : +25% HP
            .AddResult(new HealthResult(25, true)));

    /*
    * This move represents the act of running from a wild Genos
    */
    public static Move RUN = new Move("Run", "Tries to run from the opposing wild Genos")
    // Moment : Start of turn
        .AddEffect(new Effect(Moment.START_OF_TURN)
    // Result : +2 priority
                    .AddResult(new GenosStatusResult(new PriorityStatus(2), true)));

   /*
    * This move represents the act of scanning a wild Genos
    */
    public static Move SCAN = new Move("Scan", "Tries to scan the opposing wild Genos")
    // Moment : Start of turn
        .AddEffect(new Effect(Moment.START_OF_TURN)
    // Result : +2 priority
                    .AddResult(new GenosStatusResult(new PriorityStatus(2), true)));

    /*
     * This move represents the act of running from a wild Genos
     */
    protected Move(string name, string description) : base(name, description) {}

    private new Move AddEffect(Effect effect) {
        return (Move) base.AddEffect(effect);
    }
}

/*
 * This class represents a technique
 */
public class Technique : Move {

    // all techniques go here
    public static Technique ANGEL_DUST = new RangedTechnique("Angel Dust",
        "",
        Holy.HLY,
        30,
        15);

    // the class of the move
    public GenosClass TechniqueClass {get;}

    // the stamina cost of the move
    public int StaminaCost {get;}

    protected Technique(string name, string description, GenosClass techniqueClass, int staminaCost) : base(name, description) {
        this.TechniqueClass = techniqueClass;
        this.StaminaCost = staminaCost;
    }

}

/*
 * This class represents a damaging technique
 */
public class DamagingTechnique : Technique {

    // the attacking stat of the technique
    public virtual string AttackingStat {get;}

    // the defending stat of the technique
    public virtual string DefendingStat {get;}

    // the power of the move
    public int Power {get;}

    protected DamagingTechnique(string name, string description, GenosClass techniqueClass, int power, int staminaCost) : base(name, description, techniqueClass, staminaCost) {
        this.Power = power;
    }

    // returns the effectiveness of the move on the Genos
    public double Effectiveness(Genos Genos) {
        double effectiveness = 1;
        foreach(GenosClass opposingClass in Genos.Classes) {
            effectiveness = effectiveness * TechniqueClass.Effectiveness(opposingClass);
        }
        return effectiveness;
    }

}

/*
 * This class represents a meelee technique
 */
public class MeeleeTechnique : DamagingTechnique {

    public override string AttackingStat {get{return "MS";}}

    public override string DefendingStat {get{return "MR";}}

    public MeeleeTechnique(string name, string description, GenosClass techniqueClass, int power, int staminaCost) : base(name, description, techniqueClass, power, staminaCost) {}

}

/*
 * This class represents a ranged technique
 */
public class RangedTechnique : DamagingTechnique {

    public override string AttackingStat {get{return "RS";}}

    public override string DefendingStat {get{return "RR";}}

    public RangedTechnique(string name, string description, GenosClass techniqueClass, int power, int staminaCost) : base(name, description, techniqueClass, power, staminaCost) {}

}

/*
 * This class represents a support technique
 */
public class SupportTechnique : Technique {

    public SupportTechnique(string name, string description, GenosClass techniqueClass, int staminaCost) : base(name, description, techniqueClass, staminaCost) {}

}

/*
 * This class represents a switch
 */
public class Switch : Move {

    // the index we switch to
    public int Index {get;}

    public Switch(int index) : base("Switch", "Switch to index " + index) {
        this.Index = index;
        // Moment : Start of turn
        AddEffect(new Effect(Moment.START_OF_TURN)
        // Result : +2 priority (Status result)
            .AddResult(new GenosStatusResult(new PriorityStatus(2), true)));
    }
}