﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Reflection;
using UnityEngine;

/*
 * This class represents a species of Genos
 */
public class Species {

    /*
     * All the species go here
     */

    public static Species ANGIRREL = new Species(
            1,
            "Angirrel",
            new List<GenosClass>() {Holy.HLY},
            new List<Ability>() {Ability.HOLY_HEALING},
            "This angel has nascent divine powers, however, it is too young and inexperienced to make good use of them.",
            new BaseStats(40, 45, 45, 65, 50, 55),
            new Dictionary<Technique, int>() {
                {Technique.ANGEL_DUST, 1}
            });

    // The Genodex index for the species
    public int Index {get;}
    // The name of the species
    public string Name {get;}
    // The classification of the species
    public List<GenosClass> Classes {get;}
    // The abilities of the species
    public List<Ability> Abilities {get;}
    // The Genodex entry for the species
    public string Entry {get;}
    // The base stats for the species
    public BaseStats SpeciesBaseStats {get;}
    // The movepool for the species
    public Dictionary<Technique, int> Movepool {get;}
    // True if the species has been seen by the player, false otherwise
    public bool IsUnlocked {get; set;}
    // True if the species has been scanned by the player, false otherwise
    public bool IsScanned {get; set;}

    public Species(int index, 
                   String name,
                   List<GenosClass> classes,
                   List<Ability> abilities,
                   string entry, 
                   BaseStats baseStats,
                   Dictionary<Technique, int> movepool) {
        this.Index = index;
        this.Name = name;
        this.Classes = classes;
        this.Abilities = abilities;
        this.Entry = entry;
        this.Movepool = movepool;
        this.SpeciesBaseStats = baseStats;
        IsUnlocked = false;
        IsScanned = false;
    }

    /*
     * Returns either the primary or secondary ability of the Genos
     */
    public Ability GetAbility(bool first)
    {
        return first ? Abilities.First() : Abilities.LastOrDefault() == null ? Abilities.First() : Abilities.Last();
    }

}

/*
 * This class represents the base stats of a species
 */
public class BaseStats {

    // The base Health stat
    public int H { get; }
    // The base Meelee Strength stat
    public int MS { get; }
    // The base Meelee Resilience stat
    public int MR { get; }
    // The base Ranged Strength stat
    public int RS { get; }
    // The base Ranged Resilience stat
    public int RR { get; }
    // The base Reaction Time stat
    public int RT {get;}

    public BaseStats(int h, int ms, int mr, int rs, int rr, int v) {
        this.H = h;
        this.MS = ms;
        this.MR = mr;
        this.RS = rs;
        this.RR = rr;
        this.RT = v;
    }

    /*
     * Returns the base stats as an array
     */
    public int[] ToArray() {
        return new int[] {H, MS, MR, RS, RR, RT};
    }
}
